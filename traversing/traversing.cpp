#include<iostream>
#include<algorithm>
#include<string>
#include<vector>
#include<sstream>
#include<array>
using namespace std;

struct coord{
	int row;
	int column;
	char value;
};
vector<coord> points;
void encrypt(vector<vector<char>> key, vector<char> message, vector<coord> points, vector<char> cipher); //table,plain text,struct vector,cipher text
void decrypt(vector<vector<char>> key, vector<char> message, vector<coord> points, vector<char> cipher); //table,plain text,struct vector,cipher text
int main()
{
	points.push_back(coord());
	vector<char> plain,text;
	int choice;
	bool condition = true;
	char loop;
	int k, m, n, l;
	vector<vector<char> > v;
	string arr;
	string alpha = "playfirbcdeghkmnoqstuvwxz";
	/*while (condition)
	{
		cout << "Enter message: ";
		getline(cin, arr);
		cout << endl;
		cin.sync();
		cout << "1. Encrypt\n" << "2. Decrypt\n";
		cin >> choice;
		cout << endl;
		l = 0;
		k = 0, m = 0, n = 0;
		for (int i = 0; i < arr.size(); i++)
		{
			text.push_back(arr[i]);
		}
		v.resize(5);
		for (int i = 0; i < 5; ++i)
		{
			v[i].resize(5);   //allocate memory for columns
		}
		for (int i = 0; i < 5; i++)
		{
			for (int j = 0; j < 5; j++)
			{
				v[i][j] = alpha[m];
				m++;
			}
		}

		for (int i = 0; i < 5; i++)
		{
			for (int j = 0; j < 5; j++)
			{
				cout << v[i][j] << " ";
			}
			cout << endl;
		}
		cout << endl;
		switch (choice)
		{
		case 1:	encrypt(v, text, points, plain); //table,plain text,struct vector,cipher text
			cout << endl;
			break;
		case 2:	decrypt(v, text, points, plain); //table,plain text,struct vector,cipher text
			cout << endl;
			break;
			cout << endl;
		}
		cout << "Continue?\n";
		cin >> loop;
		if (loop == 'y')
		{
			condition = true;
			system("cls");
		}
		else
		{
			condition = false;
			system("cls");
		}
	}*/
	system("pause");
	return 0;
}
void encrypt(vector<vector<char>> key, vector<char> message, vector<coord> points, vector<char> cipher) //table,plain text,struct vector,cipher text
{
	int l = 0;
	int k = 0, m = 0, n = 0;
	while (k + 1 <= message.size())
	{
		for (int j = 0; j < key[0].size(); j++)
		{
			for (int i = 0; i < key[0].size(); i++)
			{
				if (key[j][i] == message[k])
				{
					points.push_back({ j, i, message[k] });
				}
			}
		}
		k += 1;
		l++;
	}
	points.erase(points.begin());
	while (n != message.size() && n + 1 != message.size())
	{
		if (points[n].row == points[n + 1].row)
		{
			cipher.push_back(key[points[n].row][(points[n].column + 1) % 5]);
			cipher.push_back(key[points[n + 1].row][(points[n + 1].column + 1) % 5]);
		}
		if (points[n].column == points[n + 1].column)
		{
			cipher.push_back(key[(points[n].row + 1) % 5][points[n].column]);
			cipher.push_back(key[(points[n + 1].row + 1) % 5][points[n + 1].column]);
		}
		if (points[n].row != points[n + 1].row && points[n].column != points[n + 1].column)
		{
			cipher.push_back(key[points[n].row][points[n + 1].column]);
			cipher.push_back(key[points[n + 1].row][points[n].column]);
		}
		n += 2;
	}
	for (int i = 0; i < cipher.size(); i++)
	{
		cout << cipher[i];
	}
}
void decrypt(vector<vector<char>> key, vector<char> message, vector<coord> points, vector<char> cipher) //table,plain text,struct vector,cipher text
{
	int l = 0;
	int k = 0, m = 0, n = 0;
	while (k + 1 <= message.size())
	{
		for (int j = 0; j < key[0].size(); j++) 
		{
			for (int i = 0; i < key[0].size(); i++)
			{
				if (key[j][i] == message[k])
				{
					points.push_back({ j, i, message[k] });
				}
			}
		}
		k += 1;
		l++;
	}
	points.erase(points.begin());
	while (n != message.size() && n + 1 != message.size())
	{
		if (points[n].row == points[n + 1].row)
		{
			cipher.push_back(key[points[n].row][((points[n].column)%5+5 - (1%5) )% 5]);
			cipher.push_back(key[points[n + 1].row][((points[n + 1].column)%5+5 - (1%5) )% 5]);
		}
		if (points[n].column == points[n + 1].column)
		{
			cipher.push_back(key[((points[n].row)%5+5 - (1%5)) % 5][points[n].column]);
			cipher.push_back(key[((points[n + 1].row)%5+5-(1%5)) % 5][points[n + 1].column]);            //cipherText.push_back(arr[((j%26)+26-(k%26))%26]);
		}
		if (points[n].row != points[n + 1].row && points[n].column != points[n + 1].column)
		{
			cipher.push_back(key[points[n].row][points[n + 1].column]);
			cipher.push_back(key[points[n + 1].row][points[n].column]);
		}
		n += 2;
	}
	for (int i = 0; i < cipher.size(); i++)
	{
		cout << cipher[i];
	}
}